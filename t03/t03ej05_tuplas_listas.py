# En Python más que arrays, lo hay son listas. 
# Lo elementos pueden ser de distinto tipo, mejor no usar
una_lista = [1, 2, 3.0, 4 + 0j, "5"]
# Las listas pueden cambiar de valor y son dinámicas. Se parece más a un ArrayList: 
una_lista_2 = [1, 2, 3.0, 4 + 0j, "5"]
una_tupla = (1, 2, 3.0, 4 + 0j, "5")

# Podemos cambiar valores en una lista. Son mutables
una_lista[2] = 3.0333
# No podemos cambiar valores en una tupla. Son inmutables
# una_tupla[2] = 3.0333

print("Elemento 1ro: {}, 2do: {}, 3ro: {}".format(una_lista[0], una_lista[1], una_lista[2]))
print("Elemento 1ro: {}, 2do: {}, 3ro: {}".format(una_tupla[0], una_tupla[1], una_tupla[2]))

# Longitud lista o tupla:
print(len(una_lista))
print(len(una_tupla))

# Assert comprueba si dos valores son iguales
assert(una_lista == una_lista_2)  # True
assert(una_lista == una_tupla)  # False
