# Mal, esto no es pythonista
a = None
if a == None:
    print('a vale None')

# Bien, esto es pythonista
a = None
if not a:
    print('a tiene un valor nulo')

b = (None, 3, "k")
if not b:
    print('b tiene un valor vacío')
else:
    print('b tiene algo')

c = "texto"
if not c:
    print('c vacío')
else:
    print('c tiene texto')