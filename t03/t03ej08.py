una_lista = [1, 2, 3.0, 4 + 0j, "5"]
una_tupla = (1, 2, 3.0, 4 + 0j, "5")

print(una_tupla[0]) #1er elemento
print(una_lista[0:3])   # Desde el 1er elemento al cuarto, exculyendo este (el 4to)

print(type(una_tupla))
print(type(una_lista))
trozo_lista = una_lista[0:2]    # Desde el 1er elemento al tercero, exculyendo este (el 3ro)
print(trozo_lista)
print(una_lista[-1])    # Último elemento
print(una_lista[:])     # Todos los elementos

arr_bidim = [
    [1, 2, 3],
    [4, 5]
]
print(arr_bidim)
print(arr_bidim[0])
print(arr_bidim[0][0])