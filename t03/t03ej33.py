# Creación de funciones: con def
def al_cuadrado(x):
    """Función que eleva un número al cuadrado."""
    y = x ** 2
    return y

print (al_cuadrado(4))
assert(al_cuadrado(4) == 16)

""" Funcion que no hace nada """
def funcion_inutil():
    pass

funcion_inutil()
