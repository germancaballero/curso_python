un_set = {1, 3, 6, 10, 15, 21, 28, 36, 45}
print(un_set)

print(un_set.pop())
print(un_set.pop())
print(un_set)

print(un_set.clear())
print(un_set)

un_set = {1, 3, 6, 10, 15, 21, 28, 36, 45}
un_set.discard(10)  # Existe
un_set.discard(11)  # No Existe, lo ignora
print(un_set)

un_set.remove(15)  # Existe
print(un_set)
un_set.remove(11)  # No Existe
