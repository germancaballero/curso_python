for ii in (1,2,3,4,5):
    print(ii)

for nombre in "Juanlu", "Siro", "Carlos":
    print(nombre)

for ii in range(3):
    print(ii)
print("2 - 5")
for jj in range(2,5):
    print(jj)

# Recorrer los números del 7 al 3, descendiente

for jj in range(7, 2, -1):
    print(jj)