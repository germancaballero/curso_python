ii = 0
while ii < 5:
    print(ii)
    ii += 1
    if ii == 4: # Con -8 sí se ejecuta el else
        break
# El bloque else se ejecuta sólo si NO ha sido interrumpido con un break
else:
    print("El bucle no ha sido interrumpido")