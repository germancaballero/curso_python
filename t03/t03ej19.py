a_set = {1,2,3,4,5,6}
b_set = {4,5,6,7,8}

print(a_set.union(b_set))
print(a_set.intersection(b_set))
print(a_set.difference(b_set))
print(b_set.difference(a_set))
# Lo contrario a intersection, equivalente al a_set XOR b_set:
print(a_set.symmetric_difference(b_set))
print(b_set.symmetric_difference(a_set))

a_lista = [1, 3, 5, 5]
b_tupla = tuple(a_lista)
c_set = set(a_lista)
print(b_tupla)
print(c_set)