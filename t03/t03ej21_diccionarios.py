# Los diccionarios es una colección, pero tienen tuplas clave:valor, en lugar de elementos sueltos.
# Como si un array en lugar acceder a los elementos por índice numérico accedieramos por clave de texto

a_dict = {
    "servidor": "bbdd.dominio.com",
    "basedatos": "MySQL",
    'usuario': 'admin',
    'password': '1234'
}
print(a_dict)
print(a_dict['servidor'])
a_dict['usuario'] = "root"
print(a_dict["usuario"])
a_dict['version'] = '8.0.1'
print(a_dict["version"])

sufijos = {
    1000: ['KB', 'MB', 'GB', 'TB'],
    1024: ['KiB', 'MiB', 'GiB', 'TiB']    
}
print(sufijos[1000])
print(sufijos[1024][1])