nombre = "Python"
cualidad = "dinámico"

print("{0} es tremendamente {1}. Vamos con {0}".format(nombre, cualidad))

array_sufijos = ['KB', "MB", "GB", 'TB']

print("1000 {0[0]} = 1 {0[1]} Comillas simples: ' o dobles \" ".format(array_sufijos))
print("1000 {0[2]} = 1 {0[3]} ".format(array_sufijos))
