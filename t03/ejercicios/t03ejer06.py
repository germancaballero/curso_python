# Crear una función para determinar si la longitud de un determinado examen cumple la normativa. Dicha normativa es: si un examen dura más de 3 horas, entonces debe tener un descanso
# Los argumentos de la función son el tiempo en horas y un valor booleano que indica si hay descanso o no. Puntos extra si lo haces en una sola línea.

def cumple_normativa(tiempo, hay_descanso ):
    # return True if tiempo < 3 and not hay_descanso else True if tiempo >= 3 and  hay_descanso else False
    if tiempo < 3 and not hay_descanso:
        return True
    else:
        if tiempo >= 3 and  hay_descanso:
            return True
        else:
            return False
    # La buena:
    # return tiempo < 3 or (tiempo > 3 and hay_descanso)

assert(cumple_normativa(2, False) is True)
assert(cumple_normativa(2, True) is False)

assert(cumple_normativa(4, True) is True)
assert(cumple_normativa(5, False) is False)
