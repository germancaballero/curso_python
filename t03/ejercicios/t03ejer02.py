# Escribir un programa que le pregunte al usuario por una palabra y le siga preguntando mientras no la acierte, o hasta un máximo de 10 veces. La comparación la puedes hacer case insensitive, y para leer la entrada de usuario, puedes usar la funcion input

PALABRA_DIV = "pythonisa"

veces = 0
while veces < 10:
    print("Pon una palabra")
    palabra = input()
    if palabra.upper() == PALABRA_DIV.upper():
        print("Adivinaste")
        break
    else:
        veces += 1
        print("Fallaste, te quedan {}".format(10-veces))
        
print("FIN")