def fib(n):
    """
    Devuelve el término n de la secuencia de Fibonacci
    @param n: posición a devolver
    @return: El término n de la secuencia
    """
    x = 1
    y = 1
    if n == 1:
        return x
    elif n == 2:
        return y
    else:
        i = 2
        while i < n: 
            f = x + y
            x, y, i = y, f, i + 1
        return f

def fib_recursivo(n):
    """
    Función recursiva que devuelve el término n de la secuencia de Fibonacci
    @param n: posición a devolver
    @return: El término n de la secuencia
    """
    if n == 0:
        return 0
    elif n == 2 or n == 1:
        return 1
    else:
        return fib_recursivo(n - 1) + fib_recursivo(n - 2)

def n_primeros(n):
    """
    Devuelve los n primeros términos de la secuencia de Fibonacci
    @param n: Número de términos a devolver
    @return: Lista con los n primeros términos
    """   
    """ F = fib_recursivo
    lista = []
    for ii in range(n):
        lista.append(F(ii))
    return lista """
    return [fib_recursivo(ii) for ii in range(n)]
# Comprobaciones
assert(fib(5) == 5)
assert(fib(6) == 8)
print(fib(7))  # 13
assert(fib_recursivo(0) == 0)
assert(fib_recursivo(3) == 2)
print("recursivo {}" . format(fib_recursivo(7)))  # 13
assert(fib(8) == 21)
assert(n_primeros(10) == [0, 1, 1, 2, 3, 5, 8, 13, 21, 34])
print(n_primeros(10))