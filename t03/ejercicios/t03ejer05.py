# Implementar la función sumatorio\_tope, de manera que sume los n primeros números hasta que lleguen al valor establecido como tope.
def sumatorio_tope(tope):
    """ Va sumando números naturales hasta llegar al tope.
    Ejemplos
    --------
    >>> sumatorio_tope(9)
    6
    # 1 + 2 + 3
    >>> sumatorio_tope(10)
    10 # 1 + 2 + 3 + 4
    >>> sumatorio_tope(12)
    10 # 1 + 2 + 3 + 4
    >>> sumatorio_tope(16)
    15 # 1 + 2 + 3 + 4 + 5
    """    
    sum = 0
    for ii in range(tope + 1):
        if sum + ii > tope:
            break
        sum += ii
    return sum
    # Tu codigo aqui
assert(sumatorio_tope(9) == 6)
assert(sumatorio_tope(10) == 10)
assert(sumatorio_tope(12) == 10)
assert(sumatorio_tope(16) == 15)