def mayor_menor_5(valor):
    return "menor" if valor < 5 else ("mayor" if valor > 5 else "igual" )

print(mayor_menor_5(4))
print(mayor_menor_5(5))
print(mayor_menor_5(6))

def compara5(n):
    return {
        True: "El número es >5",
        False: {
            True: "El número es <5",
             False: "El número es =5"
        }[n<5]
    }[n>5]

print(compara5(3))
print(compara5(5))
print(compara5(7))