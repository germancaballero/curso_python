# Implementar la función sumatorio, de manera que devuelva la suma de los n primeros números

def sumatorio(n):
    """
    Suma los n primeros números.
    Ejemplos
    --------
    >>> sumatorio(4)
    10
    """
    # Tu codigo aqui
    sum = 0
    for ii in range(n + 1):
        sum += ii
    return sum
assert(sumatorio(4) == 10)