# Mal, esto no es pythonista
numeros = range(10)
multiplos_de_3 = list() # Otra manera de declarar una lista
for element in numeros:
    print("Pos {}".format(element))
    # % es el módulo, es decir, el resto de una división entera
    if not element % 3: 
        multiplos_de_3.append(element)

print(multiplos_de_3)