def func(**kwargs):
    print("Argumentos:")
    for k,v in kwargs.items():
        print("{0} => {1}".format(k, v))

func(a=134, b=543, c="sfds")

# Si fuera un diccionario:
def func_dicc(dicc_kwargs):
    print("Argumentos:")
    for k,v in dicc_kwargs:
        print("{0} => {1}".format(k, v))
        
dicc = {"a": 134, "b": 543, "c": "sdfdf"}
func_dicc(dicc)
