x,y = 8,7

if x > y:
    print("x es mayor que y")
    print("x es el doble de y")

if x > y:
    print("x es mayor que y")
else:
    print("x es menor o igual que y")

if x < y:
    print("x es menor que y")
elif x == y:
    print("x es igual a y")
elif x == y + 1:
    print("x es igual a y + 1")
else:
    print("x es mayor que y")