frutas = {'verde': 'pera', 'amarilla': 'plátano', 'rojo': 'fresa', 'azul': 'Berenjena'}
if 'azul' in frutas:
    fruta_azul = frutas['azul']
else:
    fruta_azul = 'piña'
print(fruta_azul)
fruta_roja = frutas.get('roja', 'manzana')
print(fruta_roja)