import time
import os.path
import pygit2
from datetime import datetime
from credenciales_git import Credenciales

ruta_repo = r"C:\pruebas_pygit2\autom_repo_pruebas_py.git"
if os.path.isdir(ruta_repo):
    repo = pygit2.Repository(ruta_repo)
else:
    repo = pygit2.clone_repository(
        "https://gitlab.com/germancaballero/repo_pruebas_pygit2",
        ruta_repo)

with open(ruta_repo + "\\fich_automat.txt", "a") as fich:
    fich.write("Mira que chulada es Python {}\n".format(datetime.now()))

repo.index.add_all()
repo.index.write()
tree = repo.index.write_tree()
sig = pygit2.Signature("German C. Rguez", "email@dementira.es")

""" Creamos un commit, sobre el repositorio local (HEAD), cuyo autor
 y comitter es (sig), con un mensaje, con un índice en árbol (tree) con todos
 los ficheros añadidos, y por último una lista con el commit padre de este
 commit ([repo.head.target])
 repo.create_commit("HEAD", sig, sig, "Commit automático desde Python", tree,
 [repo.head.target]) """

cred = Credenciales()

cred_git = pygit2.UserPass(cred.usuario, cred.password)
remote = repo.remotes["origin"]
remote.credentials = cred_git
callbacks = pygit2.RemoteCallbacks(credentials=cred_git)
remote.push(["refs/heads/master"], callbacks=callbacks)
