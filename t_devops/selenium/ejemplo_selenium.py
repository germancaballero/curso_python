from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

import time

driver = webdriver.Firefox(executable_path=r"C:\Users\German\_Formacion\_Curso_Python\curso_python.git\t_devops\selenium\geckodriver.exe")
driver.maximize_window()
driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html")
print(driver.title)
assert "Selenium Easy" in driver.title

elemUserMsg = driver.find_element_by_id("user-message")
elemUserMsg.clear()
elemUserMsg.send_keys("Probando Selenium con Python")
time.sleep(2)

botonNoGracias = WebDriverWait(driver, 5).until(
    EC.presence_of_element_located((By.CSS_SELECTOR, "#at-cv-lightbox-button-holder > .at-cm-no-button"))
)
if botonNoGracias:
    botonNoGracias.click()

time.sleep(1)
elemBoton = driver.find_element_by_css_selector("#get-input > .btn")
elemBoton.click()

elemMensajeFinal = driver.find_element_by_id("display")

assert "Probando" in elemMensajeFinal.text

input("Pulsa ENTER...")
driver.close()