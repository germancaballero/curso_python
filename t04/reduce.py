import functools

lista = [1, 2, 3, 4, 5]

def suma(x, y):
    print ("x = {}, y = {}".format(x, y))
    return x + y

resultado = functools.reduce(suma, lista)

print (resultado)