import functools

lamba_suma_anteriores = lambda x,n:[x[1],x[0]+x[1]]

fib_function = lambda n:functools.reduce(lamba_suma_anteriores, range(n),[0,1])[0]

fib = [fib_function(n) for n in range(17)]
print(fib)