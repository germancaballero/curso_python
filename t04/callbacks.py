# Ejem: [3, 1, 5]  y [1, 2, 3 ]  -> [4, 3,  8]
def sumar_listas(l1, l2):
    r = []
    for index in range(len(l1)):
        r.append(l1[index] + l2[index])
    return r

def multi_listas(l1, l2):
    r = []
    for index in range(len(l1)):
        r.append(l1[index] * l2[index])
    return r

def operar_listas(l1, l2, fun_operacion):
    r = []
    for index in range(len(l1)):
        r.append( fun_operacion(l1[index], l2[index]))
    return r

print(sumar_listas([3, 1, 5] , [1, 2, 3 ] ))
print(multi_listas([3, 1, 5] , [1, 2, 3 ] ))

# Funciones callback:
def dividir(x, y):
    return x / y
def resta(x, y):
    return x - y

print (operar_listas([3, 1, 5] , [1, 2, 3 ], dividir))
print (operar_listas([3, 1, 5] , [1, 2, 3 ], resta))
print (operar_listas([6, 4, 2, 8] , [1, 2, 3, 4 ], resta))
print (operar_listas([3, 1, 5, 3] , [1, 2, 3, 2 ], lambda x, y: x ** y))