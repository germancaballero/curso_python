def suma_ultimos(x):
    return [x[1], x[0] + x[1]]

l1 = [3, 5]
s = suma_ultimos(l1)
print (l1, s)

l1 = [5, 8]
s = suma_ultimos(l1)
print (l1, s)

# Una función lambda es una función anónima, sin nombre, muchas veces de un sólo uso, no suele estar asociada a ninguna clase ni depende de nada externo (ni variables globales, ni llamadas a externas, ni valores externos, sólo depende de los parámetros), es una función idempotente.

print(lambda x: [x[1], x[0] + x[1]])