import mi_modulo
from mi_modulo import UnaClase
import unpaquete.otro_modulo as otromod

print(mi_modulo.variable)
print(mi_modulo.funcion())
# Mejor no usar variables con _
# print(mi_modulo._variable_privada)
obj = UnaClase()

otromod.funcion2()