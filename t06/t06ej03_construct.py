class MiClase:

    def __init__(self, x, y):
        self._x = x
        self.y = y

    def __str__(self):
        return "x = {0}, y={1}".format(self._x, self.y)

c = MiClase(7, 12)
print (c._x, c.y)
print("El objeto es {0} en texto".format(c))
