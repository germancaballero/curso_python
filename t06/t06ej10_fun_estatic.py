class MiClase:
    def metodo_f(self):
        print("Metodo de instancia")

    @classmethod
    def metodo_statico(*_):
        print(*_)
        print("Esto es un metodo de clase/estático")
    
MiClase.metodo_statico(10, "Ok")

obj = MiClase()
obj.metodo_f()
obj.metodo_statico()

# MiClase.metodo_f() No se puede