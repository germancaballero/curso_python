class Person:
    def __init__(self, name):
        # En esta asignación, realmente estamos llamando al setter de más abajo
        self.name = name
    @property
    def name(self):
        print("Leyendo name {}".format(self._name))
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
        print("Cambiando name {}".format(self._name))
        # Mediante el decorator @name.deleter, permitiremos eliminar una propiedad de nuestra persona
    @name.deleter
    def name(self):
        print("Eliminando name {}".format(self._name))
        del self._name

p = Person('Paco')
print(p.name)
p.name = 'Jorge'
print(p.name)
del p.name
print(p.name) # Esto provoca una excepcion, porque el campo ya no existe
