class MiClase:
    # Definición de variables miembro (atributos)
    _x = 1
    y = 2

    def saludar(self):
        print(self._x)
        print(self.y)
        print("hola")

c = MiClase()
c.saludar()
print(c._x, c.y)
c.z = 3
print(c.z)