import re

# Excepciones. Definelas aqui
class DuplicatedUsernameError(Exception):
    pass

class InvalidAgeError(Exception):
    pass

class UnderageError(Exception):
    pass

class InvalidEmailError(Exception):
    pass
class EmailMalFormadoError(Exception):
    pass

# Clase para almacenar los datos de los usuarios: nombre, edad y email

class User:
    def __init__(self, username, age, email):
        self.username = username
        self.email = email
        self.age = age

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, a):
        if int(a) < 0:
            raise InvalidAgeError("La edad no puede ser menor que cero")
        elif int(a) < 18:
            raise UnderageError("El usario es menor de 18 años")
        else:
            self._age = int(a)

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, e):
        if '@' not in e:
            raise InvalidEmailError("El email no tiene arroba")
        elif re.match( r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",e.lower()):
            raise EmailMalFormadoError("El email no tiene un formato correcto")        
        else:
            self._email = e


    def __str__(self):
        return "{} is {} years old and has the email {}".format(self.username, self.age, self.email)

example_list = [
    ("jane", "jane@example.com", 21),
    ("bob", "bob@example", 19),
    ("jane", "jane2@example.com", 25),
    ("steve", "steve@somewhere", 15),
    ("joe", "joe", 23),
    ("anna", "anna@example.com", -3),
    ("joe2", "joe@", 23),
]

directory = {}

for username, email, age in example_list:
    try:
        if username in directory.keys():
            raise DuplicatedUsernameError("El usuario {} ya existe".format(username))
        print(username)
        directory[username] = User(username, age, email)
    except Exception as e:
        print(e)

for username, u in directory.items():
    print("{} => {}".format(username, u))