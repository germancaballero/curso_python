class Foo(object):
    def __init__(self):
        self.__baz = 42

    def foo(self):
        print(self.__baz)

    def __metodo(self):
        print("Metodo privado")

class Bar(Foo):
    def __init__(self):
        super().__init__()
        self.__baz = 21

    def bar(self):
        print(self.__baz)

f = Foo()
f.foo()
print(f._Foo__baz)
f._Foo__metodo()

x = Bar()
x.bar()

print(x.__dict__)
print(x._Foo__baz)
print(x.__dict__['_Bar__baz'])
# x['_Foo__baz']  JavaScript
print("¿Qué quieres mostrar, B/F?")
letra_attr = input()
attr = '_Foo__baz' if letra_attr == 'B' else '_Bar__baz'
print(x.__dict__[attr])