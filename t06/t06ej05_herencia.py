class ClasePadre:
    def __init__(self, x, y, p):
        self._x = x # Indicamos que es propiedad privada
        self.y = y  # Público
        self.p = p

    def __str__(self):
        return "x = {0}, y={1}".format(self._x, self.y)

class ClaseOtroPadre:
    def __init__(self, a, b, p):
        self.a = a
        self.b = b
        self.p = p

    def metodo_hola(self):
        return "hola"

class ClaseHija(ClasePadre, ClaseOtroPadre):
    def __init__(self, x, y, z, a, b, p1, p2):
        # Llamamos al constructor del primer padre
        super().__init__(x, y, p1)
        #TODO: Mirar como es esto
        # ClaseOtroPadre.__init__(a, b, p2)
        self.z = z

    def __str__(self):
        return "{0}, z={1}".format(super().__str__(), self.z)

    def met_interf(self):
        print("Metodo interf en hija")

class ClaseB:
    def __init__(self, propB):
        self.propB = propB
    def met_interf(self):
        print("Metodo interf en ClaseB")
    

padre = ClasePadre(3, 4, 99)
hija = ClaseHija(5, 6, 7, 8, 9, 99, 101)


print (padre)
print (hija)
padre._x = 22
print (padre)
hija._x = 33
print (hija)
print (hija.a)
print (hija.metodo_hola())
print (hija.p)
obj_b = ClaseB(999)
print(hija.met_interf())
print(obj_b.met_interf())

obj_b.hija = hija
