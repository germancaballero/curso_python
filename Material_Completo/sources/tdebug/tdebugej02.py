import pdb


def add(x, y):
    z = x*y
    return z + 5


# Ahora la función no tiene puntos de parada
def debug_this(i1, i2):
    result = i1
    for i in range(5):
        result += i2

    result += add(i1, i2)
    return result

# Pero ya la llamo yo
pdb.runcall(debug_this, 10, 20)