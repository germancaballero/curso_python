import argparse

parser = argparse.ArgumentParser(description="Este programa calcula X^Y")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")

parser.add_argument("--base", "-b", type=int, help="la base")
parser.add_argument("--exponente", "-e", type=int, help="el exponente")


args = parser.parse_args()

if not args.base or not args.exponente:
    print("Tienes que especificar base y exponente")
    exit(-1)

answer = args.base**args.exponente

if args.quiet:
    print(answer)
elif args.verbose:
    print("{} elevado a {} es igual a {}".format(args.base, args.exponente, answer))
else:
    print("{}^{} == {}".format(args.base, args.exponente, answer))
