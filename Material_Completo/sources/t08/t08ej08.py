from functools import wraps
import logging
import os

def debug(func):
    if 'DEBUG' not in os.environ:
        return func

    log = logging.getLogger(func.__module__)
    msg = func.__qualname__
    @wraps(func)
    def wrapper(*args, **kwargs):
        log.error(msg)
        return func(*args, **kwargs)
    return wrapper

@debug
def add(x, y):
    """
        Función que suma dos números
        x: uno de los numeros
        y: el otro numero
        returns: la suma de ambos
    """
    return x + y

@debug
def sub(x, y):
    return x - y

@debug
def mul(x, y):
    return x * y

@debug
def div(x, y):
    return x / y

add(2, 2)
sub(8, 3)
mul(5, 6)
div(16, 2)

help(add)
