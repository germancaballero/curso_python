def debug(func):

    # Ojo, que qualname vino en la 3.3: https://www.python.org/dev/peps/pep-3155/
    msg = func.__qualname__
    def wrapper(*args, **kwargs):
        print('Estoy en la funcion {}'.format(msg))
        print('La funcion tiene estos argumentos:')
        print(args)
        print(kwargs)
        return func(*args, **kwargs)
    return wrapper
    
# Mejor usar esta sintáxis
@debug
def add(x, y, z=5, a='hola'):
    """
        Función que suma dos números
    """
    print(a)
    return x + y + z

res = add(2, 2, z=7, a='adios')
print(res)