import math

print("Hola que pasa curso Python")
print("¿Cuantos sois en clase?")
# Ahora pedimos un número
""" Para esto importamos la librería math
para eso usamos import """

numero = input()        # Pide un texto
numero = int(numero)    # convertimos en número
raiz = math.sqrt(numero)

print("Ufff! Qué de cuantos sois! Soy Python, el lenguaje majo")
# print("Por cierto, la raiz de " + numero + " es " + raiz)
print("Por cierto, la raiz de {} es {}".format(numero, raiz))
